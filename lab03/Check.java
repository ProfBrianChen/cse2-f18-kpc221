// Kirk Cobb
// 9/12/18
// CSE 002 Section 210

import java.util.Scanner;
public class Check{
   			public static void main(String[] args) {
        Scanner myScanner = new Scanner( System.in ); // implementing scanner
        System.out.print("Enter the original cost of the check in the form xx.xx: ");
        double checkCost = myScanner.nextDouble();    // accepting value
        System.out.print("Enter the percentage tip that you wish to pay as a whole number (in the form xx): " );
        double tipPercent = myScanner.nextDouble();   // value of tip
        tipPercent /= 100;
        System.out.print("Enter the number of people who went out to dinner: ");
        int numPeople = myScanner.nextInt();          // value for number of people at dinner

double totalCost;
double costPerPerson;
int dollars, dimes, pennies; // for storing digits to the right of the decimal point for the cost$ 
totalCost = checkCost * (1 + tipPercent);
costPerPerson = totalCost / numPeople;    // cost per person is equal to the total cost divided by number of people
dollars=(int)costPerPerson;               // total dollars in cost
dimes=(int)(costPerPerson * 10) % 10;     // total dimes in cost
pennies=(int)(costPerPerson * 100) % 10;  // total pennies in cost
System.out.println("Each person in the group owes $" + dollars + '.' + dimes + pennies);  // prints total amount each person owes

}  
  	}