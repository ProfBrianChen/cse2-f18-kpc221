// Kirk Cobb
// 9/5/18
// CSE 002 Section 210
//This program records time elapsed in seconds as well as the number of front wheel rotations during that time.
//
public class Cyclometer {
    	// main method required for every Java program
   	public static void main(String[] args) {
      int secsTrip1=480;   //measures time elapsed for trip 1 
      int secsTrip2=3220;   //measures time elapsed for trip 2
		  int countsTrip1=1561;  //measures front wheel rotations for trip 1
		  int countsTrip2=9037;  //measures front wheel rotations for trip 2
      double wheelDiameter=27.0,  //Diameter of bicycle wheel
  	PI=3.14159, //value of pi
  	feetPerMile=5280,  //value for number of feet in a mile
  	inchesPerFoot=12,   //value for number of inches in a foot
  	secondsPerMinute=60;  //value for number of seconds in a minute
      double distanceTrip1, distanceTrip2,totalDistance;  //
      System.out.println("Trip 1 took "+
       	     (secsTrip1/secondsPerMinute)+" minutes and had "+
       	      countsTrip1+" counts.");
	    System.out.println("Trip 2 took "+
       	     (secsTrip2/secondsPerMinute)+" minutes and had "+
       	      countsTrip2+" counts.");
		//The above code calculates the amount of time each trip took in minutes and the number of wheel rotations.
		//The data shows trip 1 took 8 minutes with 1561 rotations and trip 2 took 53.67 minutes with 9037 rotations.
	distanceTrip1=countsTrip1*wheelDiameter*PI;
    	// Above gives distance in inches
    	//(for each count, a rotation of the wheel travels
    	//the diameter in inches times PI)
	distanceTrip1/=inchesPerFoot*feetPerMile; // Gives distance in miles
	distanceTrip2=countsTrip2*wheelDiameter*PI/inchesPerFoot/feetPerMile;
	totalDistance=distanceTrip1+distanceTrip2;
  
  System.out.println("Trip 1 was "+distanceTrip1+" miles"); //Prints distance for trip 1 in miles
	System.out.println("Trip 2 was "+distanceTrip2+" miles"); //Prints distance for trip 2 in miles
	System.out.println("The total distance was "+totalDistance+" miles");


	}  //end of main method   
} //end of class