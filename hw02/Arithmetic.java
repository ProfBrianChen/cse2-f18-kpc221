// Kirk Cobb
// 9/11/2018
// CSE 002 Section 210

public class Arithmetic {
  public static void main(String[] args) {
    
    double paSalesTax = 0.06;     //state sales tax
    
    // Pants
    int numPants = 3;   //number of pants
    double pantsPrice = 34.98;    //price
    double pantsTotal = numPants * pantsPrice;      //price without tax
    double pantsSalesTax = pantsTotal * paSalesTax; //price with tax

    // Sweatshirts
    int numShirts = 2;  //number or sweatshirts
    double shirtPrice = 24.99;    //price
    double shirtsTotal = numShirts * shirtPrice;      //price without tax
    double shirtsSalesTax = shirtsTotal * paSalesTax;  //price with tax

    // Belts
    int numBelts = 1; //number of belts
    double beltCost = 33.99;     //price
    double beltsTotal = numBelts * beltCost;          //price without tax
    double beltsSalesTax = beltsTotal * paSalesTax;   //price with tax
    
    // Totals
    double salesTotal = pantsTotal + shirtsTotal + beltsTotal;              //total price without tax
    double salestaxTotal = pantsSalesTax + shirtsSalesTax + beltsSalesTax;  //total price of tax
    double total = salesTotal + salestaxTotal;                              //total price with tax

    System.out.println("Total costs:");   //combining total costs of all items without tax
    System.out.println("pants: " + pantsTotal);
    System.out.println("belts: " + beltsTotal);
    System.out.println("sweatshirts: " + shirtsTotal);
    
    System.out.println("\nTotal sales tax:");   //combining total taxes of all items
    System.out.println("pants: " + pantsSalesTax);
    System.out.println("belts: " + beltsSalesTax);
    System.out.println("sweatshirts: " + shirtsSalesTax);
    
    System.out.println("\nTotals:");    //combining final price of all items with tax
    System.out.println("sales: " + salesTotal);
    System.out.println("tax: " + salestaxTotal);
    System.out.println("total: " + total);
    
  }
}
