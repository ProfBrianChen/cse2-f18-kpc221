// Kirk Cobb
// 9/3/18
// CSE 002 Section 210

public class WelcomeClass {
  public static void main(String[] args) {
    ///Prints Welcome in the terminal window
    System.out.println("   -----------");
    System.out.println("   | WELCOME |");
    System.out.println("   -----------");
    System.out.println("  ^  ^  ^  ^  ^  ^");
    System.out.println(" / \\/ \\/ \\/ \\/ \\/ \\");
    System.out.println("<-K--P--C--2--2--1->");
    System.out.println(" \\ /\\ /\\ /\\ /\\ /\\ /");
    System.out.println("  v  v  v  v  v  v");
    System.out.println("My name is Kirk Cobb and I am a sophomore at Lehigh University. I enjoy long walks on the beach and boying out with my main man Latham. Have a great day!");
  }
}