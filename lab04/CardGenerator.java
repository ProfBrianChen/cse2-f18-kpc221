// Kirk Cobb
// 9/19/18
// CSE 002 Section 210

public class CardGenerator{           // main method required for every Java program
  public static void main(String[] args) {
    int cardNumber = (int)(Math.random()*(52+1))+1;   // generates a random number between 1 and 52
    String card = " ";          // creates a string to store the card that is picked
    String suit = " ";          // creates a string to store the suit of the card
          if ( cardNumber >= 1 && cardNumber <=13 ){    // makes diamonds = 1-13
          suit = "Diamonds";
          }
          else if ( cardNumber >= 14 && cardNumber <=26 ){ // makes clubs = 14-26
          suit = "Clubs";
          }
          else if ( cardNumber >= 27 && cardNumber <=39 ){  // makes hearts = 27-39
          suit = "Hearts";
          }
          else if ( cardNumber >= 40 && cardNumber <=52 ){ // makes spades = 40-52
          suit = "Spades";
          }
          int newCardNumber = (int) cardNumber % 13;    // allows card number to be found
          switch (newCardNumber){                       // determines card based on number
            case 1:
              card = "Ace";
              break;
            case 2:
              card = "2";
              break;       
            case 3:
              card = "3";
              break;       
            case 4:
              card = "4";
              break;       
            case 5:
              card = "5";
              break;       
            case 6:
              card = "6";
              break;       
            case 7:
              card = "7";
              break;       
            case 8:
              card = "8";
              break;       
            case 9:
              card = "9";
              break;       
            case 10:
              card = "10";
              break;       
            case 11:
              card = "Jack";
              break;       
            case 12:
              card = "Queen";
              break;       
            case 0:
              card = "King";
              break;
          }
    System.out.println( "You picked the " + card + " of " + suit + "." );  // combines suit and number to print what is picked
  }
}