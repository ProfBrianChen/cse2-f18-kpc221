// Kirk Cobb
// 9/18/2018
// CSE 002 Section 210

import java.util.Scanner;        // searches for scanner tool

public class Pyramid {
  public static void main(String[] args) {
    
     Scanner myScanner = new Scanner( System.in );   // express the scanner
     System.out.print("The square side of the pyramid is (input length): ");    // asks for side length
       
        double sideLength = myScanner.nextDouble();                             // next input = length
     System.out.print("The height of the pyramid is (input height): ");      // asks for length of side
        double Height = myScanner.nextDouble();                    // next input = height
        double Volume;                                             // volume
        Volume = (sideLength*sideLength) * (Height/3);             // calculate volume of pyramid
        
     System.out.println("The volume inside the pyramid is: " + Volume); // prints final volume of pyramid
  }
}