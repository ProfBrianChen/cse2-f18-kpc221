// Kirk Cobb
// 9/18/2018
// CSE 002 Section 210

import java.util.Scanner;               // searches for scanner tool
public class Convert {
  
  public static void main(String[] args) {
    
    Scanner myScanner = new Scanner(System.in);                 // express scanner
    System.out.print("Enter the affected area in acres: ");     // asks for affected area
    
       double Acres = myScanner.nextDouble();      // calculation for acres
    System.out.print("Enter the rainfall in the affected area: ");    // asks for amount of rainfall
     
       double rainInches = myScanner.nextDouble();
       double Rain;    // rain integer
       double Miles;   // miles integer
       double cubicMiles;    // rain over mile integer
   
     Miles = Acres / 640.00;                   // converting acres of rain to miles
     Rain = rainInches * 0.0000157828;         // converting inches of rain to miles
     cubicMiles = Rain * Miles;                // calculating amount of rain over miles
    System.out.println(cubicMiles + " cubic miles");    // prints the amont of rain over cubic miles
    
  }
}