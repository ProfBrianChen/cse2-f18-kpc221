// Kirk Cobb
// 10/23/18
// CSE 002 Section 210

import java.util.Scanner;     //imports scanner tool
public class EncryptedX {
  public static void main (String [] args) {
    Scanner scan = new Scanner (System.in);                              //makes instance and scanner
    System.out.println ("Enter an integer between 0-100 (inclusive).");  //prompts user for input
    int value = 0;
    boolean validInput = false;
  
    while (!validInput){                //loop for correct input
      if (scan.hasNextInt()){
        value = scan.nextInt();
        if (value>(-1) && value<101){   //creates parameters for correct input
          validInput = true;
        }
        else{
          System.out.println("Invalid input, try again");    //requests new input
        }
      }
      else{
        System.out.println("Invalid input, try again");     //requests new input
        scan.next();
      }
    }
    
    for(int i = 0; i < value; i++){       //loops j
      for(int j = 0; j < value; j++){     //loops i
        if (i == j || i == value - j - 1){
          System.out.print(" ");          //prints section of negative-space X
        } else{
          System.out.print("X");          //prints given number of X's
        }
      }
      System.out.println();
    }
  }
}